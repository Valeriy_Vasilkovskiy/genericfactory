package main;

public class Folder <N,P> implements ToMainFabric {
    @Override
    public ToMainFabric createObj(int key) {
        switch (key) {
            case 1:
                return new <N> NormalPeople();
            case 2:
                return new <P> PapichSub();
        }
        return null;
    }
}
