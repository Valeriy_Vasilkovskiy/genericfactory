package main;

public class POVSEDNEVNOST implements ForNormal{
    private String name = "Vlad";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void sayGoodMorning() {
        System.out.println(getName() + " Good Morning");
    }
}
